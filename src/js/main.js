$(document).ready(function(){

   /* Анимация placeholder (плавное перемещение placeholder вверх при фокусе) */
   {
    $('.form-input')
        .each(function () {
            if ($(this).val()) {
                $(this).addClass("is__valid");
            }
        })
        .on('focus', function () {
            $(this).addClass('is__valid');
        })
        .on('focusout', function () {
            let inputHasValue = $(this).val();

            if (inputHasValue) {
                $(this).addClass('is__valid');
            } else {
                $(this).removeClass('is__valid');
            }
        });

    $('.form-select').on('change', function () {
        if ($(this).val() || ($(this).val() !=='')) {
            $(this).addClass('is__valid');
        } else {
            $(this).removeClass('is__valid');
        }
    });
   }

   
    /* Маски ввода*/
    {
        $(".phoneMask").inputmask("+7 (999) 999-99-99", {
            showMaskOnHover: false,
            removeMaskOnSubmit: true,
            onUnMask: function (maskedValue, unmaskedValue) {
                var phone = maskedValue.replace(/[^\d]/g, '');
                return phone;
            }
        });
    }

    $("form").submit(function(e){
        e.preventDefault();

        $.ajax({
            beforeSend: function() {
                $('.modal__wrapper.loader').show();
            },
            type: "POST",
            url: 'callback.php',
            data: $(this).serialize(),
            success: function (response) {
                $(".begin").css("display", "none");
                $(".done").css("display", "block");
            },
            error: function (response) {
                console.log('error');
                console.log(response);
            },
            complete: function() {
                $('.modal__wrapper.loader').hide();
            }
        })
    });

       /* Закрытие модалки при клике на OK */
       $('.done-btn').on('click', function (e) {        
        $('.modal__wrapper').fadeOut();
    });
    
    $(".header_burger").on("click", function(e){
        $(".mobile-nav").css("display", "block")
    })

    //клик на бургер вызывает мобильный nav
    $('.header_burger').on('click', function(e) {
        $('.right-side').toggleClass('mob-nav')
        $('.header_burger').toggleClass('change-burger')
        $('body').toggleClass('notscroll')
        $('.right-side ul').toggleClass('show-ul')
        $('.right-side .get-qr-btn').toggleClass('show-btn')
    })

    //при клике на пункты nav в мобилке
    $('.remove-class').on('click', function(e) {
        $('.right-side').removeClass('mob-nav')
        $('.header_burger').removeClass('change-burger')
        $('body').removeClass('notscroll')
        $('.right-side ul').removeClass('show-ul')
        $('.right-side .get-qr-btn').removeClass('show-btn')
    })

    //при клике на кнопку nav в мобилке
    $('.get-qr-btn.show-btn').on('click', function(e) {
        $('.right-side').removeClass('mob-nav')
        $('.header_burger').removeClass('change-burger')
        $('body').removeClass('notscroll')
        $('.right-side ul').removeClass('show-ul')
        $('.right-side .get-qr-btn').removeClass('show-btn')
    })
});