<?php

include "TMail.php";

if ($_POST) {

    $email = htmlspecialchars($_POST["email"]);
    $phone = htmlspecialchars($_POST["phone"] ? $_POST["phone"] : '');
    $name = htmlspecialchars($_POST["name"] ? $_POST["name"] : '');
    $domain = htmlspecialchars($_POST["domain"] ? $_POST["domain"] : '' );

    $message = 'Заказ обратного звонка на номер ' . $phone
        . '. Имя клиента: ' . $name
        . '. Email: ' . $email
        . '. Домен: ' . $domain;

    $json = array();
    if (!$name or !$phone or !$email) {
        $json['error'] = 'Вы зaпoлнили нe всe пoля.';
        echo json_encode($json);
        die();
    }

    // Кодировка для отправленных писем
    function mime_header_encode($str, $data_charset, $send_charset) {
        if($data_charset != $send_charset)
            $str=iconv($data_charset,$send_charset.'//IGNORE',$str);
        return ('=?'.$send_charset.'?B?'.base64_encode($str).'?=');
    }

    $emailgo = new TEmail;
    $emailgo->from_email = 'inqarfood@gmail.com';
    $emailgo->from_name  = 'qrmenu.inqar-food.kz';
    $emailgo->to_email   = 'tasen.kan@mail.ru';
    $emailgo->to_name    = 'InqarSoft';
    $emailgo->subject    = 'Обратный звонок';
    $emailgo->body       = $message;

    if ($emailgo->send()) {
        $json['error'] = 0;
    } else {
        $json['error'] = 'Fail';
    };

    echo json_encode($json);
} else {
    header('Location: /');
}
?>