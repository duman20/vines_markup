const gulp = require('gulp');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const del = require('del');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');
const gcmq = require('gulp-group-css-media-queries');
const babel = require("gulp-babel");


const styleFiles = [
   './src/css/assets/normalize.scss',
   './src/css/main.scss'
]

const scriptFiles = [
   './src/js/assets/jquery-3.4.1.min.js',
   './src/js/assets/jquery.inputmask.min.js',
   './src/js/main.js'
]

gulp.task('styles', () => {
   return gulp.src(styleFiles)
      .pipe(sass())
      .pipe(concat('style.css'))
      .pipe(autoprefixer({
         overrideBrowserslist: ['> 0.01%'],
         cascade: false
      }))
      .pipe(gcmq())
      .pipe(cleanCSS({
         level: 2
      }))
      .pipe(rename({
         suffix: '.min'
      }))
      .pipe(gulp.dest('./build/css'))
      .pipe(browserSync.stream());
});


gulp.task('scripts', () => {

   return gulp.src(scriptFiles)
      .pipe(babel())
      .pipe(concat('main.js'))
      .pipe(uglify({
         toplevel: true
      }))
      .pipe(rename({
         suffix: '.min'
      }))
      .pipe(gulp.dest('./build/js'))
      .pipe(browserSync.stream());
});


gulp.task('del', () => {
   return del(['build/*'])
});

gulp.task('img-compress', ()=> {
   return gulp.src('./src/img/**')
   // .pipe(imagemin({
   //    progressive: true
   // }))
   .pipe(gulp.dest('./build/img/'))
});

gulp.task('fonts', ()=> {
   return gulp.src('./src/fonts/**')
   .pipe(gulp.dest('./build/fonts/'))
});

gulp.task('watch', () => {
   browserSync.init({
      server: {
         baseDir: "./"
      }
   });
   gulp.watch('./src/img/**', gulp.series('img-compress'))
   gulp.watch('./src/fonts/**', gulp.series('fonts'))
   gulp.watch('./src/css/**/*.scss', gulp.series('styles'))
   gulp.watch('./src/js/**/*.js', gulp.series('scripts'))
   gulp.watch("./*.html").on('change', browserSync.reload);
   
});


gulp.task('default', gulp.series('del', gulp.parallel('styles', 'scripts', 'img-compress', 'fonts'), 'watch'));
